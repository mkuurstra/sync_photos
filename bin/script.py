import argparse
import logging
import sys

from sync_photos import Photos


def debugger_is_active() -> bool:
    """Return if the debugger is currently active"""
    return hasattr(sys, "gettrace") and sys.gettrace() is not None


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--debug", action="store_true")
    args = parser.parse_args()

    # Setup basic logging to screen
    logging.getLogger().handlers.clear()
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    formatter = logging.Formatter(
        "%(asctime)s - %(levelname)s - %(name)s - %(message)s"
    )
    ch.setFormatter(formatter)

    photos = Photos()
    if args.debug or debugger_is_active():
        logger = logging.getLogger("sync_photos")
        logger.handlers.clear()
        logger.addHandler(ch)
        logger.setLevel(logging.DEBUG)
        # Requirement osxphotos disables logging for debug, re-enable it
        logging.disable(logging.NOTSET)
    else:
        logger = logging.getLogger("sync_photos")
        logger.handlers.clear()
        logger.addHandler(ch)

    photos.sync_photos("2012")


if __name__ == "__main__":
    main()
