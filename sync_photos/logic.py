import datetime
import logging
from typing import Iterator

import exiftool
import osxphotos

log = logging.getLogger(__name__)


def init_photosdb(path: str = "") -> osxphotos.photosdb.photosdb.PhotosDB:
    if path:
        photosdb = osxphotos.PhotosDB(path)
    else:
        photosdb = osxphotos.PhotosDB(osxphotos.utils.get_last_library_path())
    return photosdb


def walk_folders(
    photosdb: osxphotos.photosdb.photosdb.PhotosDB | osxphotos.albuminfo.FolderInfo,
) -> osxphotos.albuminfo.FolderInfo:
    if type(photosdb) == osxphotos.photosdb.photosdb.PhotosDB:
        container = photosdb.folder_info
    elif type(photosdb) == osxphotos.albuminfo.FolderInfo:
        container = photosdb.subfolders
    else:
        raise NotImplementedError

    for folder in container:
        yield folder
        yield from walk_folders(folder)


def walk_albums(
    photosdb: osxphotos.photosdb.photosdb.PhotosDB
    | osxphotos.albuminfo.FolderInfo
    | list[osxphotos.albuminfo.FolderInfo],
) -> Iterator[osxphotos.albuminfo.AlbumInfo]:
    if type(photosdb) == list:
        for item in photosdb:
            yield from walk_albums(item)
    elif (
        type(photosdb) == osxphotos.photosdb.photosdb.PhotosDB
        or type(photosdb) == osxphotos.albuminfo.FolderInfo
    ):
        for album in photosdb.album_info:
            yield album
    else:
        raise NotImplementedError


def get_folders_by_name(
    photosdb: osxphotos.photosdb.photosdb.PhotosDB | osxphotos.albuminfo.FolderInfo,
    folder_names: str | list[str],
) -> list[osxphotos.albuminfo.FolderInfo]:
    if type(folder_names) == list:
        wanted_folders = folder_names
    elif type(folder_names) == str:
        wanted_folders = [folder_names]
    else:
        raise NotImplementedError
    ret = []
    for folder in walk_folders(photosdb):
        if folder.title in wanted_folders:
            ret.append(folder)
    if ret:
        return ret
    else:
        raise LookupError


def walk_photos(
    albums: osxphotos.albuminfo.AlbumInfo | list[osxphotos.albuminfo.AlbumInfo],
) -> Iterator[osxphotos.photoinfo.PhotoInfo]:
    if type(albums) == list:
        for album in albums:
            yield from walk_photos(album)
    elif type(albums) == osxphotos.albuminfo.AlbumInfo:
        album = albums
        for photo in album.photos:
            yield photo
    else:
        raise NotImplementedError


def get_exif_dates(image: str) -> dict[str, datetime.datetime]:
    with exiftool.ExifToolHelper() as et:
        metadata = et.get_tags(
            image,
            [
                "DateTimeOriginal",
                "CreateDate",
            ],
        )
    entry = metadata[0]
    entry.pop("SourceFile")

    # Fix alternative notation type yyyy-mm-dd instead of yyyy:mm:dd
    entry["EXIF:CreateDate"] = entry["EXIF:CreateDate"].replace("-", ":")
    entry["EXIF:DateTimeOriginal"] = entry["EXIF:DateTimeOriginal"].replace("-", ":")

    if entry:
        entry["EXIF:CreateDate"] = datetime.datetime.strptime(
            entry["EXIF:CreateDate"], "%Y:%m:%d %H:%M:%S"
        )
        entry["EXIF:DateTimeOriginal"] = datetime.datetime.strptime(
            entry["EXIF:DateTimeOriginal"], "%Y:%m:%d %H:%M:%S"
        )
        return entry
    else:
        raise LookupError


def is_different_date(date1: datetime.datetime, date2: datetime.datetime) -> bool:
    date1_standardized = date1.strftime("%Y-%m-%d %H:%M:%S")
    date2_standardized = date2.strftime("%Y-%m-%d %H:%M:%S")
    if date1_standardized != date2_standardized:
        log.debug(f"'{date1}' differs from '{date2}'")
        return True
    return False


def set_exif_dates(image: str, date: datetime.datetime) -> None:
    new_date = date.strftime("%Y:%m:%d %H:%M:%S")
    with exiftool.ExifToolHelper() as et:
        et.set_tags(
            image,
            tags={
                "DateTimeOriginal": new_date,
                "CreateDate": new_date,
            },
            params=["-P", "-overwrite_original"],
        )


def is_missing_exif_dates(image: str) -> bool:
    try:
        exif_dates = get_exif_dates(image)
    except LookupError:
        return True
    if "EXIF:CreateDate" not in exif_dates:
        return True
    return False
