import logging
import os

from . import logic


class Photos:
    def __init__(self, photosdb_path="") -> None:
        self.photosdb = logic.init_photosdb(photosdb_path)
        self.log = logging.getLogger(__name__ + ".Photos")

    def sync_photos(self, folder: str | None = None) -> None:
        if folder:
            folders = logic.get_folders_by_name(self.photosdb, folder)
            folder_name = folder
        else:
            folders = self.photosdb
            folder_name = "all"

        self.log.info(f"Starting sync for folders: {folder_name}")
        for album in logic.walk_albums(folders):
            self.log.info(f"Starting sync for album: {album.title}")
            for photo in logic.walk_photos(album):
                self.log.debug(f"Starting sync for photo: {photo.filename}")
                if not photo.path:
                    # Photos seems to remove path when it detects a file is gone
                    self.log.warning(
                        f"Photo is probably deleted: ({photo.albums[0]}) {photo.filename}"
                    )
                    continue
                elif not os.path.isfile(photo.path):
                    # If Photos still points to a path that does not exist anymore it is
                    # deleted for sure
                    self.log.warning(
                        f"Photo is deleted: ({photo.albums[0]}) {photo.filename}"
                    )
                    continue
                elif photo.isphoto and logic.is_missing_exif_dates(photo.path):
                    # If a foto does not have EXIF dates, update them
                    self.log.warning(
                        f"Photo is missing EXIF info: ({photo.albums[0]}) {photo.filename}"
                    )
                    logic.set_exif_dates(photo.path, photo.date)
                    continue

                if photo.isphoto and logic.is_different_date(
                    photo.date,
                    logic.get_exif_dates(photo.path)["EXIF:DateTimeOriginal"],
                ):
                    self.log.warning(
                        f"Date does not match EXIF: ({photo.albums[0]}) {photo.filename}"
                    )
