import pytest

from sync_photos import ui

test_photosdb_path = "test/test_library.photoslibrary"
photos = ui.Photos(test_photosdb_path)


def test_sync_photos() -> None:
    photos.sync_photos()


if __name__ == "__main__":
    pytest.main()
