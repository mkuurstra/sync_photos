import datetime

import osxphotos
import pytest

from sync_photos import logic

test_photosdb_path = "test/test_library.photoslibrary"
photosdb = osxphotos.PhotosDB(test_photosdb_path)


def test_walk_folders() -> None:
    folders = [x.title for x in logic.walk_folders(photosdb)]
    assert folders == ["folder1", "folder2", "subfolder", "empty_folder"]


def test_walk_albums() -> None:
    albums = [x.title for x in logic.walk_albums(photosdb)]
    assert albums == ["album", "album_in_folder", "album_in_subfolder"]


def test_get_folders_by_name() -> None:
    # Lookup by string
    folders = [x.title for x in logic.get_folders_by_name(photosdb, "folder1")]
    assert folders == ["folder1"]
    # Lookup by list with single item
    folders = [x.title for x in logic.get_folders_by_name(photosdb, ["empty_folder"])]
    assert folders == ["empty_folder"]
    # Lookup by list with multiple items
    folders = [
        x.title for x in logic.get_folders_by_name(photosdb, ["folder1", "subfolder"])
    ]
    assert folders == ["folder1", "subfolder"]


def test_walk_photos() -> None:
    folder = list(logic.get_folders_by_name(photosdb, "folder1"))[0]
    album = list(logic.walk_albums(folder))[0]
    photos = [x.filename for x in logic.walk_photos(album)]
    assert photos == ["photo3.jpg", "photo2.jpg", "photo1.jpg"]


def test_set_and_get_exif_dates() -> None:
    photo_without_exif = "test/photo_without_exif.jpg"
    with open(photo_without_exif, "rb") as file_handle:
        image_data = file_handle.read()
    assert logic.is_missing_exif_dates(photo_without_exif)
    new_date = datetime.datetime.now()
    less_accurate_new_date = datetime.datetime.strptime(
        new_date.strftime("%Y:%m:%d %H:%M:%S"), "%Y:%m:%d %H:%M:%S"
    )
    logic.set_exif_dates(photo_without_exif, new_date)
    assert not logic.is_missing_exif_dates(photo_without_exif)
    photo_dates = logic.get_exif_dates(photo_without_exif)
    for k, v in photo_dates.items():
        assert less_accurate_new_date == v
    with open(photo_without_exif, "wb") as file_handle:
        file_handle.write(image_data)


def test_different_date() -> None:
    # Test if the same dates return true
    date1 = datetime.datetime.now()
    date2 = datetime.datetime.now()
    assert not logic.is_different_date(date1, date2)
    # It shouldn't matter if a date is missing milliseconds
    date1 = datetime.datetime.strptime(
        date1.strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S"
    )
    assert not logic.is_different_date(date1, date2)
    # When second date is changed, result should be false
    date2 = datetime.datetime.now() - datetime.timedelta(hours=1)
    assert logic.is_different_date(date1, date2)


if __name__ == "__main__":
    pytest.main()
